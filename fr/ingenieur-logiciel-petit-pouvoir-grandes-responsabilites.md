#Ingénieur logiciel: un petit pouvoir, de grandes responsabilités


Objectifs contradictoires, visions produit divergentes, manque de maturité sur l’agilité, rétention d’information, incompréhension des besoins, tout autant de problèmes qui mènent à des contextes ultra politisés où chacun défend ses propres intérêts. Les équipes peinent alors à interagir mettant en risque le projet. Les organisations, méthodologies et leurs garants sont souvent ceux pris pour responsable de ces dérives. 

Il est vrai qu’en tant qu’ingénieur logiciel, notre champs d’action a tendance à être limité aux sujets techniques. Mais sommes nous vraiment étranger à ce qui pourrait s’apparenter à des problèmes d’organisation ? Nos décisions techniques n’ont-elles pas une influence plus large que le métier auquel elles répondent ?
Examinons le cas de Saint Marcellin Distribution (SMD), une entreprise spécialisée dans la vente de fromages dans le monde.


# Il était une fois Saint Marcellin Distribution

Saint Marcellin Distribution, c’est une belle histoire de famille, d’apéro et de copains ! 
La famille Fromoureux produit ses premiers Saint Marcellin il y a quelques dizaines d’années de cela, en Isère. Au début destinés aux apéros entre copains et à la kermesse des enfants, le petit fromage devient très vite célèbre dans la région. Personne du coin n’imagine son repas entre amis sans le Saint Marcellin des Fromoureux.
La famille Fromoureux décide alors d’acheter une crémerie et de commercialiser son fameux fromage: « Les Marcellins de Fromoureux » est née.
La demande explosant, l’entreprise se structure à l’aide de multiples rachats de crémeries régionales. Son organisation et son optimisation de la distribution lui permet alors d’acheminer son doux mets partout en France.

Les années passent, et les rachats successifs permettent à l’entreprise de se diversifier et d’étendre son offre à une multitude de fromages différents, partout dans le monde. 
Alors que les ordinateurs ne sont  encore que des outils dédiés aux entreprises, Saint Marcellin Distribution comprend rapidement l’intérêt du numérique et de l’avantage compétitif que cela peut lui donner. Elle initie alors son premier projet: Srelas.
Les technologies sont à la pointe pour l’époque: Cobol et autres mainframes IBM. SMD gagne des parts de marchés, et le numérique lui permet d’optimiser ses coûts et processus, aussi bien sur les aspects production que livraison.

Mais la technologie évolue à vitesse V et SMD se fait peu à peu rattraper par ses concurrents, au point de passer de précurseur à dinosaure de la tech. Il est alors temps pour eux de réagir
. C’est à grands coups de billets que l’entreprise décide de repenser toute sa stratégie numérique. Expérience utilisateurs, suivis client, réactivité, design thinking, micro services, tout autant de mots qui résonnent jusqu’au comité de direction de SMD et qui alimentent sa volonté de se réinviter sur la scène de la grande distribution.
L'appel d’offre pour le projet New Srelas est lancé.


# Les premiers choix techniques

Gestion des stocks, encaissement, fiches clients, administration de la production, les sujets sont multiples et les ERP prétendant pouvoir répondre à leurs besoins tout aussi nombreux. SMD comprend cependant que ces processus standardisés ne correspondent pas à l’image qu’ils ont d’une expérience utilisateur réussie. Un  grand gagnant émerge alors de cet appel d’offre: SAP. SMD décide de se référer à eux pour tous les aspects métier stratégique comme la gestion des stocks, l'approvisionnement et les flux financiers. 

Mais SAP devra travailler avec des experts de l’expérience utilisateurs et des architecture modernes qui auront pour objectif de construire une interface sexy, ergonomique et performante et ce dans un seul but: réduire le coût de formation des nouveaux collaborateurs en facilitant la prise en main des nouveaux outils. 

Pour réaliser cette interface, SMD demande donc les services de différentes 

# Le début des problèmes

A métiers différents, compétences et méthodes différentes. Agilité d’un côté, cycle V de l’autre. Cadre ultra fermé pour l’un, possibilités infinies pour l’autre.  Mais chacune des deux équipes en est-elle seulement conscience ?

Les premières fonctionnalités sont développées sans accrocs.



Et si on s’y prenait autrement ?
— proposition d’une archi légèrement différente (CQRS), levier actionnables et arguments coopération (facilitation à obtenir des efforts de la part des autres)



# Conclusion

Avec du recule, on observe que certains problèmes de Saint Marcellin auraient pu être adressés en amont avec des choix techniques légèrement différents. Mais c’est pourtant cette petite différence qui aurait pu grandement simplifier la collaboration entre les équipes et par conséquent permettre à tous de se concentrer sur la réalisation du produit.

La politique et les défauts organisationnels font partie intégrante des entreprises dans lesquelles nous sommes amenés à intervenir. Ces contraintes, bien que détachées du besoin initial, sont au moins tout autant structurantes.
La loi de Conway a mis en évidence la relation entre la structure de communication des organisations et le design des systèmes d’information. On pourrait alors se cacher derrière celle-ci pour blâmer une organisation inadaptée. C’est peut-être le cas, mais en tant qu’ingénieur logiciel nous ne sommes pas en mesure de challenger les entreprises sur ce point. Notre expertise nous permet cependant de travailler autour de ces contraintes à condition que celles-ci soient connues. 

Il est alors grand temps que nous prenions la mesure de la réelle portée de nos choix techniques et que ces derniers se fassent en pleine conscience de l’environnement périphérique au projet.

